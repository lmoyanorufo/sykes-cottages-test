Sykes Cottages Interview
==============
[![SykesLogo](http://careers.sykescottages.co.uk/images/logo.png)](http://sykes-cottages.projectsoldiers.com)

It's a searcher to find cottages for holidays.

Instalation
---------------

At least PHP 5.5 or greater and MySQL 5.5 or greater. Init file is included in config folder.
It's necessary define environment vars in the VirtualHost (example included in project) file.

The project is based in Silex, a microframework, the small brother of Symfony.

Example
```
	SetEnv SYKES_COTTAGES_DDBB sykes_interview
	SetEnv SYKES_COTTAGES_DDBB_USER sykes_interview
	SetEnv SYKES_COTTAGES_DDBB_PASS sykes_interview
	SetEnv SYKES_COTTAGES_DDBB_HOST localhost
```


To init the database use init.sql in config directory.
Also its necesarry install dependencies (composer install)

There are two test defined executables with phpunit, it's necessary have
an user and a database with this credentials.

```
database = sykes_interview
user = sykes_interview
pass = sykes_interview
host = localhost
```

Things to improve
------------------
Logging
Fails control
Unit testing
Use pimple for dependency injection.
Improve search (Improve query or use search engine like elasticSearch)
...


You can view the demo here [link](http://ticketea.projectsoldiers.com)
