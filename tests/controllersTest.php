<?php

use Silex\WebTestCase;

class controllersTest extends WebTestCase
{
    public function testGetHomepage()
    {   
        $client = $this->createClient();
        $client->followRedirects(true);
        $crawler = $client->request('GET', '/');
                
        $this->assertTrue($client->getResponse()->isOk());
        $this->assertContains('Find properties', $crawler->filter('body')->text());
    }
    
    public function testGetHomepageWithNoResults()
    {   
        $client = $this->createClient();
        $client->followRedirects(true);
        $crawler = $client->request('GET', '/?sleeps=10000');
                
        $this->assertTrue($client->getResponse()->isOk());
        $this->assertContains('We have found 0 properties', $crawler->filter('body')->text());
    }

    public function createApplication()
    {
        $this->initTestEnvironment();
        $app = require __DIR__.'/../src/app.php';
        require __DIR__.'/../config/prod.php';
        require __DIR__.'/../src/controllers.php';
        $app['session.test'] = true;

        return $this->app = $app;
    }
    
    protected function initTestEnvironment()
    {
        putenv("SYKES_COTTAGES_DDBB=sykes_interview");
        putenv("SYKES_COTTAGES_DDBB_USER=sykes_interview");
        putenv("SYKES_COTTAGES_DDBB_PASS=sykes_interview");
        putenv("SYKES_COTTAGES_DDBB_HOST=localhost");
    }
}
