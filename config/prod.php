<?php

// configure your app for the production environment

$app['twig.path'] = array(__DIR__.'/../templates');
$app['twig.options'] = array('cache' => __DIR__.'/../var/cache/twig');
$app['debug'] = true; // Remove this in production.


/* General configuration */

$app['numResultsPerPage'] = 5;
$app['dateFormat'] = 'Y-m-d';

$app['minSleeps'] = 1;
$app['minBeds'] = 1;


// This variables should be calculated each time a property is added or deleted.
$app['maxSleeps'] = 6;
$app['maxBeds'] = 10;
