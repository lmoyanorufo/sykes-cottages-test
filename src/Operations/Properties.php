<?php

namespace Operations;

use Searchs\PropertySearch;
use Searchs\ResultSearch;


class Properties
{   
    protected $propertiesMapper;

    public function __construct($propertiesMapper)
    {
        $this->propertiesMapper = $propertiesMapper;
    }
    
    public function search(PropertySearch $search)
    {        
        $listSearch = $this->propertiesMapper->getSearchResults($search);
        $totalSearch = $this->propertiesMapper->getSearchCount($search);

        return new ResultSearch(
            $listSearch, $totalSearch
        );
    }
}
