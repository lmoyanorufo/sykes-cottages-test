<?php

namespace Searchs;

abstract class Search
{
    protected $limit;
    protected $offset;
    
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }
    
    public function setOffset($offset)
    {
        $this->offset = $offset;
    }
    
    function getLimit() {
        return $this->limit;
    }

    function getOffset() {
        return $this->offset;
    }


}