<?php

namespace Searchs;

class PropertySearch extends Search {

    protected $locationName = null;
    protected $nearTheBeach = null;
    protected $acceptPets = null;
    protected $numBeds = null;
    protected $numSleeps = null;
    protected $availability = null;

    function setLocationName($locationName) 
    {
        $this->locationName = $locationName;
    }

    function setNearTheBeach($nearTheBeach) 
    {
        $this->nearTheBeach = $nearTheBeach;
    }

    function setAcceptPets($acceptPets) 
    {
        $this->acceptPets = $acceptPets;
    }

    function setNumBeds($numBeds) 
    {
        $this->numBeds = $numBeds;
    }

    function setNumSleeps($numSleeps) 
    {
        $this->numSleeps = $numSleeps;
    }
    
    function setAvailability(Availability $availability) 
    {
        $this->availability = $availability;
    }
    
    function getLocationName() 
    {
        return $this->locationName;
    }

    function getNearTheBeach() 
    {
        return $this->nearTheBeach;
    }

    function getAcceptPets() 
    {
        return $this->acceptPets;
    }

    function getNumBeds() 
    {
        return $this->numBeds;
    }

    function getNumSleeps() 
    {
        return $this->numSleeps;
    }
    
    /**
     * 
     * @return Availability
     */
    function getAvailability()
    {
        return $this->availability;
    }
}
