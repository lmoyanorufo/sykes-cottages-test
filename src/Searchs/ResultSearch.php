<?php

namespace Searchs;

class ResultSearch 
{

    protected $listResults;
    protected $totalResults;

    public function __construct($listResults, $totalResults) 
    {
        $this->listResults = $listResults;
        $this->totalResults = $totalResults;
    }

    public function getListResults() 
    {
        return $this->listResults;
    }

    public function getTotalResults() 
    {
        return $this->totalResults;
    }

}
