<?php

namespace Searchs;

class Availability{

    protected $startDate = null;
    protected $endDate = null;
    
    public function __construct($startDate, $endDate, $format) 
    {   
        $this->startDate =  \DateTime::createFromFormat($format, $startDate);
        $this->endDate = \DateTime::createFromFormat($format, $endDate);
    }
    
    /**
     * 
     * @return \DateTime
     */
    public function getStartDate() 
    {
        return $this->startDate;
    }
    
    /**
     * 
     * @return \DateTime
     */
    public function getEndDate() 
    {
        return $this->endDate;
    }
    
    public function hasCoherence() 
    {        
        $validDates = ($this->startDate instanceof \DateTime) &&
                      ($this->endDate instanceof \DateTime);
        
        if (!$validDates){
            return false;
        }
        
        try {
            $timestampStart = $this->startDate->getTimestamp();
            $timestampEnd = $this->endDate->getTimestamp();

            $dayTs = 24 * 60 * 60;
            $diff = $timestampEnd - $timestampStart;

            return $timestampStart < $timestampEnd && $diff > $dayTs;
        } catch (\Exception $ex) {
            return false;
        }
    }
}
