<?php

use Silex\Application;
use Silex\Provider\AssetServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;

$app = new Application();
$app->register(new ServiceControllerServiceProvider());
$app->register(new AssetServiceProvider());
$app->register(new TwigServiceProvider());
$app->register(new HttpFragmentServiceProvider());
$app['twig'] = $app->extend('twig', function ($twig, $app) {

    return $twig;
});

$ddbbName = getenv("SYKES_COTTAGES_DDBB");
$ddbbUser = getenv("SYKES_COTTAGES_DDBB_USER");
$ddbbPass = getenv("SYKES_COTTAGES_DDBB_PASS");
$ddbbHost = getenv("SYKES_COTTAGES_DDBB_HOST");

$app->register(new Silex\Provider\DoctrineServiceProvider(), [
    'db.options' => [
        'driver' => 'pdo_mysql',
        'host' => $ddbbHost,
        'dbname' => $ddbbName,
        'user' => $ddbbUser,
        'password' => $ddbbPass,
        'charset' => 'utf8mb4'
    ]
]);

return $app;
