<?php

namespace Controllers;

use Symfony\Component\HttpFoundation\Request;
use Operations\OperationsBuilder;
use Searchs\PropertySearch;
use Searchs\ResultSearch;
use Searchs\Availability;

class Properties {

    protected $app;
    protected $propertyOperations;
        
    public function __construct($app) 
    {
        $this->app = $app;
        $operationsBuilder = OperationsBuilder::getInstance($app);
        $this->propertyOperations = $operationsBuilder->getOperations("Properties");
    }

    public function indexAction(Request $request) 
    {
        $search = $this->getSearchFromRequest($request);
        $resultSearch = $this->propertyOperations->search($search);
        $params = $this->getParamsView($request, $resultSearch, $search);
        
        return $this->app['twig']->render('index.html.twig', $params);
    }
    
    protected function getParamsView(Request $request, ResultSearch $resultSearch, PropertySearch $search)
    {   
        $listQueryParams = $request->query->all();
        $filteredParams = [];
        
        foreach($listQueryParams as $key => $value){
            
            if ($key == 'page' || empty($value)){
                continue;
            }
            $value = urldecode($value);
            $filteredParams[$key] = urlencode($value);
        }
        
        $urlHasParams = count($filteredParams);
        $urlPager = $this->app['url_generator']->generate('main', $filteredParams);
        $numPages = ceil($resultSearch->getTotalResults() / $this->app['numResultsPerPage']);
        
        $numPage = $request->query->get("page", 1);
        
        $availability = $search->getAvailability();
        $valueStartDate = "";
        $valueEndDate = "";
        
        if ($availability->hasCoherence()){
            $valueStartDate = $availability->getStartDate()->format($this->app['dateFormat']);
            $valueEndDate = $availability->getEndDate()->format($this->app['dateFormat']);
        }
        
        $params = [
            'minSleeps' => $this->app['minSleeps'],
            'maxSleeps' => $this->app['maxSleeps'],
            'minBeds' => $this->app['minBeds'],
            'maxBeds' => $this->app['maxBeds'],
            'numPages' => $numPages,
            'currentPage' => $numPage,
            'urlPager' => $urlPager,
            'urlPagerHasParams' => $urlHasParams,
            'numProperties' => $resultSearch->getTotalResults(),
            'properties' => $resultSearch->getListResults(),
            'locationName' => null !== $search->getLocationName()? $search->getLocationName() : "", 
            'nearTheBeach' => null !== $search->getNearTheBeach()? $search->getNearTheBeach() : false,    
            'acceptPets' => null !== $search->getAcceptPets()? $search->getAcceptPets() : false,   
            'numSleeps' => null !== $search->getNumSleeps()? $search->getNumSleeps() : $this->app['minSleeps'],    
            'numBeds' => null !== $search->getNumBeds()? $search->getNumBeds() : $this->app['minBeds'],
            'startDate' => $valueStartDate,
            'endDate' => $valueEndDate
        ];
        
        return $params;
    }
    
    protected function getSearchFromRequest(Request $request)
    {
        $numPage = $request->query->get("page", 1);
        $locationName = urldecode($request->query->get("locationName", null));
        $nearTheBeach = $request->query->has("nearTheBeach");
        $acceptPets = $request->query->has("acceptsPets");
        $numSleeps = $request->query->get("sleeps", null);
        $numBeds = $request->query->get("beds", null);
        $startDate = $request->query->get("startDate", null);
        $endDate = $request->query->get("endDate", null);
        $offset = ($numPage - 1) * $this->app['numResultsPerPage'];
        $availability = new Availability($startDate, $endDate, $this->app['dateFormat']);

        
        // Build the search.
        $search = new PropertySearch();
        $search->setLocationName($locationName);
        $search->setAcceptPets($acceptPets);
        $search->setNumBeds($numBeds);
        $search->setNumSleeps($numSleeps);
        $search->setNearTheBeach($nearTheBeach);
        $search->setLimit($this->app['numResultsPerPage']);
        $search->setOffset($offset);
        $search->setAvailability($availability);
        
        return $search;
    }
}
