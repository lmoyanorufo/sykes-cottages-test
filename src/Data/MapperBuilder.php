<?php 

namespace Data;

use Data\BeanFactory;
	
class MapperBuilder
{	
	private static $instance = null;
	protected $app;

	private function __construct($app)
	{	
		$this->app = $app;
	}

	public function getMapper($nameClass)
	{		
		$className = 'Data\\Mappers\\'.$nameClass;
		$classExists = class_exists($className);

		if (!$classExists){
			throw new \Exception("MAPPER_DOESNT_EXISTS"); 
		}

		return new $className($this->app['db'], $this->app, BeanFactory::getInstance());
	}

	public static function getInstance($app)
	{
		if (null === self::$instance){
			self::$instance = new MapperBuilder($app);
		}

		return self::$instance;
	}
}