<?php 

namespace Data;
	
class BeanFactory
{	
	private static $instance = null;
	protected $app;

	public function getBean($nameClass)
	{		
		$className = 'Data\\Beans\\'.$nameClass;
		$classExists = class_exists($className);

		if (!$classExists){
			throw new \Exception("BEAN_DOESNT_EXISTS"); 
		}

		return new $className();
	}

	public static function getInstance()
	{
		if (null === self::$instance){
			self::$instance = new BeanFactory();
		}

		return self::$instance;
	}
}