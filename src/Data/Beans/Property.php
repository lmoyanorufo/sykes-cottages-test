<?php

namespace Data\Beans;

class Property extends Bean
{
    protected $id;
    protected $locationId;
    protected $propertyName;
    protected $nearBeach;
    protected $acceptPets;
    protected $numSleeps;
    protected $numBeds;

    public function fillFromRow($row) 
    {
        $this->id = $row['__pk'];
        $this->locationId = $row['_fk_location'];
        $this->propertyName = $row['property_name'];
        $this->nearBeach = $row['near_beach'];
        $this->acceptPets = $row['accepet_pets'];
        $this->numSleeps = $row['sleeps'];
        $this->numBeds = $row['beds'];
    }
    
    public function getId() 
    {
        return $this->id;
    }

    public function getLocationId() 
    {
        return $this->locationId;
    }

    public function getPropertyName() 
    {
        return $this->propertyName;
    }

    public function getNearBeach() 
    {
        return $this->nearBeach;
    }

    public function getAcceptPets() 
    {
        return $this->acceptPets;
    }

    public function getNumSleeps() 
    {
        return $this->numSleeps;
    }

    public function getNumBeds() 
    {
        return $this->numBeds;
    }
}
