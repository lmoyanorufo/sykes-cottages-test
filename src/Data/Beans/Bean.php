<?php

namespace Data\Beans;

abstract class Bean
{
    public abstract function fillFromRow($row);
}