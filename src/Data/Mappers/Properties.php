<?php

namespace Data\Mappers;

use Searchs\PropertySearch;
use PDO;

class Properties extends Mapper {
    
    public function getSearchCount(PropertySearch $search)
    {
        $baseQuery = "SELECT count(*) as count from properties p ";
        $stmt = $this->getStatement($search, $baseQuery, false);
        
        return $this->executeAndGetCount($stmt);
    }
    
    public function getSearchResults(PropertySearch $search)
    {
        $baseQuery = "SELECT p.* from properties p ";
        $stmt = $this->getStatement($search, $baseQuery, true);
        
        return $this->executeAndGetResults($stmt, 'Property');
    }
    
    protected function getStatement(PropertySearch $search, $baseQuery, $paged)
    {   
        $valuesToBind = [];
        $wheres = [];

        if (!empty($search->getLocationName())) {
            $baseQuery.= " LEFT JOIN locations l ON p._fk_location = l.__pk ";
            $wheres[] = "l.location_name LIKE :locationName";

            $valuesToBind[] = [
                'name' => 'locationName',
                'value' => '%'.$search->getLocationName().'%',
                'type' => PDO::PARAM_STR
            ];
        }
        
        $availability = $search->getAvailability();
        
        if ($availability->hasCoherence()) {
            
            $conditionStartsColision = ' (b.start_date >= :startDate AND b.start_date <= :endDate)';
            $conditionEndsColision = ' (b.end_date >= :startDate AND b.end_date <= :endDate)';
            $conditionsInvolve = ' (b.start_date <= :startDate AND b.end_date >= :endDate)';
            $currentBookings = "( ".implode(" OR ", [$conditionStartsColision, $conditionEndsColision, $conditionsInvolve]). ")";            
            $conditionAvailability = "p.__pk NOT IN (SELECT distinct _fk_property from bookings b WHERE ".$currentBookings." )";

            $startDate = $availability->getStartDate()->format($this->app['dateFormat']);
            $endDate = $availability->getEndDate()->format($this->app['dateFormat']);
            
            $valuesToBind[] = [
                'name' => 'startDate',
                'value' => $startDate,
                'type' => PDO::PARAM_STR
            ];
            
            $valuesToBind[] = [
                'name' => 'endDate',
                'value' => $endDate,
                'type' => PDO::PARAM_STR
            ];
            
            $wheres[] = $conditionAvailability;
        }
        
                
        if (!empty($search->getAcceptPets())) {
            
            $wheres[] = 'p.accepet_pets = :acceptPets';
            $valuesToBind[] = [
                'name' => 'acceptPets',
                'value' => $search->getAcceptPets(),
                'type' => PDO::PARAM_BOOL
            ];
        }
        
        if (!empty($search->getNearTheBeach())) {
            
            $wheres[] = 'p.near_beach = :nearBeach';
            $valuesToBind[] = [
                'name' => 'nearBeach',
                'value' => $search->getNearTheBeach(),
                'type' => PDO::PARAM_BOOL
            ];
        }
        
        if (!empty($search->getNumSleeps())) {
            
            $wheres[] = 'p.sleeps >= :sleeps';
            $valuesToBind[] = [
                'name' => 'sleeps',
                'value' => $search->getNumSleeps(),
                'type' => PDO::PARAM_INT
            ];
        }
        
         if (!empty($search->getNumBeds())) {
            
            $wheres[] = 'p.beds >= :beds';
            $valuesToBind[] = [
                'name' => 'beds',
                'value' => $search->getNumBeds(),
                'type' => PDO::PARAM_INT
            ];
        }
        
        $limitAndOffset = "";
        if ($paged){
            $limitAndOffset = " LIMIT :offset,:limit";
        
            $valuesToBind[] = [
                'name' => 'limit',
                'value' => $search->getLimit(),
                'type' => PDO::PARAM_INT
            ];

            $valuesToBind[] = [
                'name' => 'offset',
                'value' => $search->getOffset(),
                'type' => PDO::PARAM_INT
            ];
        }

        $implodeWheres = count($wheres) ? " WHERE ".implode(' AND ', $wheres) : " ";
        
        $fullQuery = $baseQuery . $implodeWheres . $limitAndOffset;

        $stmt = $this->db->prepare($fullQuery);
        
        foreach($valuesToBind as $valueToBind){
            $stmt->bindValue($valueToBind['name'], $valueToBind['value']  ,$valueToBind['type']);
        }
        
        return $stmt;
    }
}
