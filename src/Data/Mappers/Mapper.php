<?php

namespace Data\Mappers;

use Data\BeanFactory;

abstract class Mapper
{
    protected $db;
    protected $app;
    protected $beanFactory;

    public function __construct($db, $app, $beanFactory)
    {
        $this->db = $db;
        $this->app = $app;
        $this->beanFactory = $beanFactory;
    }
    
    protected function executeAndGetResults($stmt, $beanName)
    {
        $stmt->execute();
        $results = $stmt->fetchAll();

        $return = [];
        foreach ($results as $result) {

            $bean = $this->beanFactory->getBean($beanName);
            $bean->fillFromRow($result);
            $return[] = $bean;
        }

        return $return;
    }
    
    protected function executeAndGetCount($stmt)
    {
        $stmt->execute();
        $result = $stmt->fetch();
        
        return $result['count'];
    }
}
